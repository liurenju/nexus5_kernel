cmd_arch/arm/boot/msm8974-hammerhead-rev-bn.dtb := /home/liurenju/Desktop/wear_research/Nexus5-kernel/msm/scripts/dtc/dtc -O dtb -o arch/arm/boot/msm8974-hammerhead-rev-bn.dtb -b 0  -d arch/arm/boot/.msm8974-hammerhead-rev-bn.dtb.d arch/arm/boot/dts/msm8974-hammerhead-rev-bn.dts

source_arch/arm/boot/msm8974-hammerhead-rev-bn.dtb := arch/arm/boot/dts/msm8974-hammerhead-rev-bn.dts

deps_arch/arm/boot/msm8974-hammerhead-rev-bn.dtb := \
  arch/arm/boot/dts/msm8974-lge-common/msm8974-v2-lge.dtsi \
  arch/arm/boot/dts/msm8974-lge-common/../msm8974-v2.dtsi \
  arch/arm/boot/dts/msm8974-lge-common/../msm8974.dtsi \
  arch/arm/boot/dts/msm8974-lge-common/../skeleton.dtsi \
  arch/arm/boot/dts/msm8974-lge-common/../msm8974-camera.dtsi \
  arch/arm/boot/dts/msm8974-lge-common/../msm8974-coresight.dtsi \
  arch/arm/boot/dts/msm8974-lge-common/../msm-gdsc.dtsi \
  arch/arm/boot/dts/msm8974-lge-common/../msm8974-ion.dtsi \
  arch/arm/boot/dts/msm8974-lge-common/../msm8974-gpu.dtsi \
  arch/arm/boot/dts/msm8974-lge-common/../msm8974-mdss.dtsi \
  arch/arm/boot/dts/msm8974-lge-common/../msm8974-smp2p.dtsi \
  arch/arm/boot/dts/msm8974-lge-common/../msm8974-bus.dtsi \
  arch/arm/boot/dts/msm8974-lge-common/../msm-pm8x41-rpm-regulator.dtsi \
  arch/arm/boot/dts/msm8974-lge-common/../msm-pm8841.dtsi \
  arch/arm/boot/dts/msm8974-lge-common/../msm-pm8941.dtsi \
  arch/arm/boot/dts/msm8974-lge-common/../msm8974-regulator.dtsi \
  arch/arm/boot/dts/msm8974-lge-common/../msm8974-clock.dtsi \
  arch/arm/boot/dts/msm8974-lge-common/../msm8974-v2-iommu.dtsi \
  arch/arm/boot/dts/msm8974-lge-common/../msm-iommu-v1.dtsi \
  arch/arm/boot/dts/msm8974-lge-common/../msm8974-v2-iommu-domains.dtsi \
  arch/arm/boot/dts/msm8974-lge-common/../msm8974-v2-pm.dtsi \
  arch/arm/boot/dts/msm8974-lge-common/msm8974-lge-common.dtsi \
  arch/arm/boot/dts/msm8974-lge-common/../msm8974-leds.dtsi \
  arch/arm/boot/dts/msm8974-lge-common/msm8974-lge-panel.dtsi \
  arch/arm/boot/dts/msm8974-lge-common/msm8974-lge-input.dtsi \
  arch/arm/boot/dts/msm8974-lge-common/msm8974-lge-hdmi.dtsi \
  arch/arm/boot/dts/msm8974-lge-common/msm8974-lge-usb.dtsi \
  arch/arm/boot/dts/msm8974-lge-common/msm8974-lge-misc.dtsi \
  arch/arm/boot/dts/msm8974-lge-common/msm8974-lge-pm.dtsi \
  arch/arm/boot/dts/msm8974-hammerhead/msm8974-hammerhead-rev-bn.dtsi \
  arch/arm/boot/dts/msm8974-hammerhead/msm8974-hammerhead.dtsi \
  arch/arm/boot/dts/msm8974-hammerhead/msm8974-hammerhead-panel.dtsi \
  arch/arm/boot/dts/msm8974-hammerhead/msm8974-hammerhead-input.dtsi \
  arch/arm/boot/dts/msm8974-hammerhead/msm8974-hammerhead-hdmi.dtsi \
  arch/arm/boot/dts/msm8974-hammerhead/msm8974-hammerhead-usb.dtsi \
  arch/arm/boot/dts/msm8974-hammerhead/msm8974-hammerhead-misc.dtsi \
  arch/arm/boot/dts/msm8974-hammerhead/msm8974-hammerhead-pm.dtsi \
  arch/arm/boot/dts/msm8974-hammerhead/msm8974-hammerhead-camera.dtsi \
  arch/arm/boot/dts/msm8974-hammerhead/msm8974-hammerhead-nfc.dtsi \
  arch/arm/boot/dts/msm8974-hammerhead/msm8974-hammerhead-regulator.dtsi \
  arch/arm/boot/dts/msm8974-hammerhead/msm8974-hammerhead-sensor.dtsi \

arch/arm/boot/msm8974-hammerhead-rev-bn.dtb: $(deps_arch/arm/boot/msm8974-hammerhead-rev-bn.dtb)

$(deps_arch/arm/boot/msm8974-hammerhead-rev-bn.dtb):
